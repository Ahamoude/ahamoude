<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome to the Zoo</title>
</head>
<body>

    <?php
    $heure = date("H");
    
    $imagePath = '';

    if ($heure >= 0 && $heure < 12) {
        $imagePath = 'zebreglasses.jpg';
    } elseif ($heure >= 12 && $heure < 18) {
        $imagePath = 'girrafe.jpg';
    } else {
        $imagePath = 'dumydkyx1l6a1.jpg';
    }
    ?>    

    <img src="<?php echo $imagePath; ?>" alt="Animal">
    <h1>Welcome to the Zoo</h1>
</body>
</html>
