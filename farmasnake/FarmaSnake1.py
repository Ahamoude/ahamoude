def demander_nombre():
    nombre = input("Entrez un nombre : ")
    return nombre

def verif_nombre():
    nombre = int(input("Entrez un nombre entre 0 et 255 : "))
    if 0 <= nombre <= 255:
        print("Correct")
    else:
        print("Incorrect")