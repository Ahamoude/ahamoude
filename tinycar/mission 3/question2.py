n = int(input("Entrez le nombre de produits à traiter : "))  # Demande à l'utilisateur le nombre de produits

for i in range(n):  # Boucle pour chaque produit
    marque = input("Entrez la marque de la voiture : ")
    modele = input("Entrez le modèle de la voiture : ")
    prix_ht = float(input("Entrez le prix HT de la voiture : "))

    taux_tva = 0.20
    prix_ttc = prix_ht * (1 + taux_tva)

    if prix_ttc > 20000:
        remise = prix_ttc * 0.10
        prix_ttc = prix_ttc - remise

    print("Marque de la voiture :", marque)
    print("Modèle de la voiture :", modele)
    print("Le prix TTC à payer est de", prix_ttc, "euros.")

    if prix_ttc < prix_ht:
        print("Une remise de 10 % a été appliquée.")

print("Mission accomplie ! Merci de votre participation.")
