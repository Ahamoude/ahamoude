# Demande à l'utilisateur de saisir le prix HT de la voiture
prix_ht = float(input(" Entrez le prix HT de la voiture : "))

# Définition du taux de TVA
taux_tva = 0.2

# Calcul du prix TTC
prix_ttc = prix_ht * (1 + taux_tva)

# Affichage du prix TTC à payer
print("Le prix TTC est ", prix_ttc, "euros.")
