# Définition des variables
prix_ht = 20000
taux_tva = 0.2 
# Calcul du prix TTC
prix_ttc = prix_ht * (1 + taux_tva)

# Affichage du résultat
print("Prix HT de la voiture :", prix_ht, "euros")
print("Taux de TVA :", taux_tva * 100, "%")
print("Prix TTC de la voiture :", prix_ttc, "euros")
