code_secret = "Padawan"  
entree_code = input("Entrez le code secret pour accéder à l'application: ")

if entree_code == code_secret:
    print("Accès autorisé. Vous pouvez utiliser l'application.")

    print("Choisissez une option de carte de fidélité :")
    print("1 - Sans carte")
    print("2 - Avec carte gold")
    print("3 - Avec carte platinium")
    
    choix_carte = int(input("Entrez le numéro correspondant à votre choix: "))

    reduction = 0.0

    if choix_carte == 2:
        reduction = 20.0
    elif choix_carte == 3:
        reduction = 15.0
    elif choix_carte != 1:
        print("Choix de carte invalide. Aucune réduction appliquée.")

    marque = input("Veuillez entrer la marque de la voiture: ")
    modele = input("Veuillez entrer le modèle de la voiture: ")
    prix_ht = float(input("Entrez le prix HT de la voiture: "))

    print("Le produit est-il électrique?")
    est_electrique = input("Entrez 'oui' si c'est électrique, 'non' sinon: ").lower()

    taux_tva_electrique = 0.05
    taux_tva_normal = 0.20
    if est_electrique == 'oui':
        taux_tva = taux_tva_electrique
    else:
        taux_tva = taux_tva_normal

    prix_ttc = prix_ht * (1 + taux_tva) * (1 - (reduction / 100))
    if prix_ttc > 20000:
        remise = prix_ttc * 0.10
        prix_ttc = prix_ttc - remise

    print("Marque de la voiture:", marque)
    print("Modèle de la voiture:", modele)
    print("Le prix TTC à payer est de", prix_ttc, "euros.")

    if prix_ttc < prix_ht:
        print("Une remise a été appliquée.")
else:
    print("Code secret incorrect. Accès refusé.")
