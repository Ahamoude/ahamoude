marque = input("Veuillez entrer la marque de la voiture : ")
modele = input("Veuillez entrer le modèle de la voiture : ")
prix_ht = float(input("Entrez le prix HT de la voiture : "))

print("Le produit est-il électrique ?")
est_electrique = input("Entrez 'oui' si c'est électrique, 'non' sinon : ").lower()

taux_tva_electrique = 0.05
taux_tva_normal = 0.20
if est_electrique == 'oui':
    taux_tva = taux_tva_electrique
else:
    taux_tva = taux_tva_normal

prix_ttc = prix_ht * (1 + taux_tva)

if prix_ttc > 20000:
    remise = prix_ttc * 0.10
    prix_ttc = prix_ttc - remise

print("Marque de la voiture :", marque)
print("Modèle de la voiture :", modele)
print("Le prix TTC à payer est de", prix_ttc, "euros.")

if prix_ttc < prix_ht:
    print("Une remise de 10 % a été appliquée.")
