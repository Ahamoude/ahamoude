piecesvoiture = ["rétroviseurs", "jantes", "feux", "sieges", "embrayage"]
prixHT = []

for piece in piecesvoiture:
    prix_ht = float(input(f"Entrez le prix HT de l'accessoire '{piece}': "))
    prixHT.append(prix_ht)

for i in range(len(piecesvoiture)):
    piece = piecesvoiture[i]
    prix_ht = prixHT[i]
    print(f"Pièces : {piece} -> Prix HT : {prix_ht} €")
