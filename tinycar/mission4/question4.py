def main():
    taille_panier = int(input("Entrez la taille du panier : "))
    piecesvoiture = []
    prixHT = []

    for i in range(taille_panier):
        nom_accessoire = input(f"Entrez le nom de l'accessoire {i+1}: ")
        prix_ht = float(input(f"Entrez le prix HT de {nom_accessoire}: "))
        piecesvoiture.append(nom_accessoire)
        prixHT.append(prix_ht)

    print("\nListe des accessoires dans le panier :")
    for piece, prix_ht in zip(piecesvoiture, prixHT):
        print(f"Pièce : {piece} -> Prix HT : {prix_ht} €")

    somme_totale = sum(prixHT)
    print(f"\nSomme totale des achats : {somme_totale} €")

    index_min = prixHT.index(min(prixHT))
    print(f"\nAccessoire le moins cher : {piecesvoiture[index_min]} -> Prix : {prixHT[index_min]} €")

    index_max = prixHT.index(max(prixHT))
    print(f"Accessoire le plus cher : {piecesvoiture[index_max]} -> Prix : {prixHT[index_max]} €")

    prix_moyen = somme_totale / taille_panier
    print(f"Prix moyen des accessoires : {prix_moyen} €")

if __name__ == "__main__":
    main()
