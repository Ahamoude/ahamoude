piecesvoiture = ["rétroviseurs", "jantes","feux", "sieges","embrayage"]
prixHT = [50.0, 200.0, 5.0, 150.0, 250.0]

for i in range(len(piecesvoiture)):
    nom = piecesvoiture[i]
    prix_ht = prixHT[i]
    print(f"Pièces : {nom} -> Prix HT : {prix_ht} €")
