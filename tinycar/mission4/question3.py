def main():
    piecesvoiture = ["rétroviseurs", "jantes", "feux", "sieges", "embrayage"]
    prixHT = []

    for piece in piecesvoiture:
        prix_ht = float(input(f"Entrez le prix de l'accessoire '{piece}': "))
        prixHT.append(prix_ht)

    print("\nListe des accessoires dans le panier :")
    for i in range(len(piecesvoiture)):
        piece = piecesvoiture[i]
        prix_ht = prixHT[i]
        print(f"Pièce : {piece} -> Prix HT : {prix_ht} €")

    somme_totale = sum(prixHT)
    print(f"\nSomme totale des achats : {somme_totale} €")

if __name__ == "__main__":
    main()
