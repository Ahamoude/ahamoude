def calculer_prix_ttc(prix_ht: float) -> float:
    tva = 0.20
    prix_ttc = prix_ht * (1 + tva)
    return prix_ttc

piecesvoiture = ["rétroviseurs", "jantes", "feux", "sieges", "embrayage"]
prixHT = [50.0, 200.0, 5.0, 150.0, 250.0]
prixTTC = []

for i in range(len(piecesvoiture)):
    prix_ht = prixHT[i]
    prix_ttc = calculer_prix_ttc(prix_ht)
    prixTTC.append(prix_ttc)

for i in range(len(piecesvoiture)):
    nom = piecesvoiture[i]
    prix_ht = prixHT[i]
    prix_ttc = prixTTC[i]
    print(f"Accessoire : {nom} -> Prix HT : {prix_ht} € -> Prix TTC : {prix_ttc} €")
