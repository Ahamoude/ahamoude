def calculer_moyenne_prix(prix_ht: list) -> float:
    moyenne = sum(prix_ht) / len(prix_ht)
    return moyenne

def trouver_prix_ht_minimal(prix_ht: list) -> float:
    prix_minimal = min(prix_ht)
    return prix_minimal

def trouver_prix_ht_maximal(prix_ht: list) -> float:
    prix_maximal = max(prix_ht)
    return prix_maximal

moyenne_prix_ht = calculer_moyenne_prix(prixHT)
prix_ht_minimal = trouver_prix_ht_minimal(prixHT)
prix_ht_maximal = trouver_prix_ht_maximal(prixHT)

print(f"Moyenne des prix HT : {moyenne_prix_ht} €")
print(f"Prix HT minimal : {prix_ht_minimal} €")
print(f"Prix HT maximal : {prix_ht_maximal} €")
