def afficher_description_accessoire(nom: str, prix_ht: float) -> None:
    print(f"Accessoire : {nom} -> Prix HT : {prix_ht} €")

piecesvoiture = ["rétroviseurs", "jantes", "feux", "sieges", "embrayage"]
prixHT = [50.0, 200.0, 5.0, 150.0, 250.0]

for i in range(len(piecesvoiture)):
    nom = piecesvoiture[i]
    prix_ht = prixHT[i]
    afficher_description_accessoire(nom, prix_ht)
