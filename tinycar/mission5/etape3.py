def afficher_elements(pieces: list, prix_ht: list, prix_ttc: list) -> None:
    for i in range(len(pieces)):
        nom = pieces[i]
        prix_ht_element = prix_ht[i]
        prix_ttc_element = prix_ttc[i]
        print(f"Accessoire : {nom} -> Prix HT : {prix_ht_element} € -> Prix TTC : {prix_ttc_element} €")

piecesvoiture = ["rétroviseurs", "jantes", "feux", "sieges", "embrayage"]
prixHT = [50.0, 200.0, 5.0, 150.0, 250.0]
prixTTC = []

for i in range(len(piecesvoiture)):
    prix_ht = prixHT[i]
    prix_ttc = calculer_prix_ttc(prix_ht)
    prixTTC.append(prix_ttc)

afficher_elements(piecesvoiture, prixHT, prixTTC)
